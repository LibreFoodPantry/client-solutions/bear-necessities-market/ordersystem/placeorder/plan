# VSCode Extension Installation Instructions
*  Docker extension - Allows for building, managing and deploying applications from VSCode and can be used for debugging Node.js files. 
Install docker: click extensions->search "docker"->click install.
Docker's extension id: ms-azuretools.vscode-docker
*  ESLint extension - Allows for the integration of ESLint into VSCode. It uses ESLint library which is installed in both the Frontend and Backend Folders. 
Install ESLint: click extensions->search "ESLint"->click install.
ESLint's extension id: dbaeumer.vscode-eslint
*  GitLens (Git supercharged) extension - Enhances the Git capabilities that are already built into VSCode as well as helps with code authorship.
Install GitLens - click extensions->search "GitLens"->click install.
Gitlens's extension id: eamodio.gitlens
*  Live Server - Used for launching application on local server with live reload feature. 
Install Live Server: click extensions->search "Live Server"->click install.
Live Server's extension id: ritwickdey.liveserver
Usage: right click the index.html file in the Frontend's public folder->Select "Open with Live Server"