# Working Agreement for the Scrum Plums

  

## Members and Roles:

### Communities of Practice:

Nihar Parikh - REST API *expert*

Thomas Richard - User Interface *expert*

Dan MacDonald - Backend *expert*

Mark Meade - Build system / pipeline *expert*

### Other Roles:

Scrum Master - Mark Meade

Team Spy - Rotates

  

## Meeting Logistics:

  

- Team meetings are to be held Wednesdays at 4:00 PM

- Stand Up meetings are to be held **during class time** on Tuesdays and Thursdays, unless being replaced by another meeting.

- Each team member is responsible for **3 to 4** work hours / week **outside** of class and meetings.

- Sprint Plannings are to be held in class.

- Backlog Groomings are to be held within a 3 day period prior to all sprints

- Sprint Retrospectives are to be held within a 3 day period preceding all sprints

  

## Sprint Logistics:

  

- Sprints Consist *on Average* of 32 story points. Story Points are assigned on the following scale:

- 0 Points:

	- A ticket that takes <15 minutes.

	- Can be completed by 1 person.

- 1 Point:

	- A ticket that takes 1 full working day*.

	- Can be completed by 1 person, or 2 people in half a day*.

- 2 Points:

	- A ticket that takes 2 full working days*.

	- Can be completed by 1 person, or 2 people in a day*

- 4 Points:

	- A ticket that takes a full working week.

	- Can be completed by 1 person one the whole week, or 2 in half.

- 8 Points:

	- A ticket that takes two full working weeks.

	- Should only be completed by two people over 1 week.

*note : For academic purposes, 1 working day = ~1 hour of work.

  

- 3 Sprints are to be held during the Semester:

	- Sprint 1: February 4 - February 23 (5 work sessions in class)

	- Sprint 2: March 2 - March 25 (5 work sessions in class)

	- Sprint 3: April 6 - April 27 (5 work sessions in class)

- Each member of the team is expected to attend every meeting, unless they have other commitments.

	- In this case, it is their responsibility to get the information missed from teammates that attended the meetings.

- A grace period of **5** minutes exists at the start of every meeting

Note: these meetings are **NOT** in class meetings, but out of class.

  
  

## General Practices:

### Code Review:

  

- Tickets that require a pull request **must** be reviewed by **at least** 1/2 of the team. This is to ensure that:

	- The code reads as if it has 1 author.

	- The code stays consistent.

	- The code does not contain bugs.

	- The code does not break.

	- The code review process will be as follows:

		- Tickets consisting of a **single** programmer will require **2** approvals; neither of the approvers can be the programmer.

		- Tickets consisting of **two** programmers will also require **2** approvals, *however*, **one** of the reviewers may be a programmer on the ticket.

	- The programmer is responsible for finding a teammate to review their code.

  

Example Review Process:

- The Programmer(s) finish the code, and move it into the "needs review 6/8" workflow.

- They should create a new pull request with an appropriate title for the ticket/branch they are working on.

- They then contact teammate(s) to let them know that their code is ready for review.

- Should the code **NOT pass** review, that's okay! That's what review is for!

- The **reviewer** should move the code back to the "doing task 5/8" flow. and let the programmer(s) know.

- The **reviewer** should explain why it didn't pass in the comments of the PR

- The **reviewer** should create tasks in the comments of the PR that must be completed in order to make the PR pass code review the next time it is proposed.

- Should the code **PASS** review, congratulations!

- The **reviewer** should move the code into the "needs merge 7/8" flow, and let the programmer(s) know.

- The programmers should perform all necessary git commands on their local branch before merging to the remote. This can be complicated. **If you are unfamiliar with merging git branches, or feel like something could go wrong, ask a teammate for help**.

  

### General Coding:

All code must have consistent styling. For best practice, when working in a file, follow the syntax/ styling of the code already there. If you notice something that seems out of place / something that could be improved **make a ticket**. If you notice different stylings across files, **make a ticket**. It is important to receive credit for your time, and closing tickets is the easiest way of showing effort. 

Creating tickets also leads to more exposure for an issue, helping to get more eyes on it, giving more alternatives.


  

### Kanban Board

The kanban board is an organizational tool to help programmers keep track of tickets as they progress from the backlog to closed. The workflows are as follows:

- Backlog (1/8)

	- Was not accepted for sprint in a planning meeting. If you think it should be worked on in the next sprint, move to proposed and help groom it.

- Proposed (2/8)

	- Someone thinks this should be in the next sprint. Team grooms. PO prioritizes. Reviewed for acceptance in next planning meeting.

- Revise (3/8)

	- Needs MINOR revisions (documented on issue) before being accepted into the sprint. Must be revised by due date. On due date, PO moves to "to do" or "backlog" and removes due date.

- Task to do (4/8)

	- A task to do. Tasks should be SMART (see https://xp123.com/articles/invest-in-good-stories-and-smart-tasks/)

- Doing task (5/8)

	- A claimed task actively being worked on.

- Needs review (6/8)

	- The work for this issue needs a review.

- Needs Merge (7/8)

	- Linked MRs have been reviewed and need to be merged.

- Done (8/8)

	- This issue has been "implemented".

- Po accepted (9/8)

	- Product Owner accepted as done.

  

### I have a ticket... now what?

The whole process can seem a bit daunting at first and differ from person to person, so hopefully following this convention will simplify things.

  

### Miscanelous:

1. Create a branch on GitLab in the appropriate Repository Where the issue exists. Name this branch **COME UP WITH NAMING CONVENTION BASED ON ISSUE**.

2. Pull down the new branch on your local machine.
- `git fetch && git pull`

3. Check out the branch.
- `git checkout **Branch_Name**`
- You'll know that you've done the previous steps corectly when the terminal says that the branch is set up to track it's remote counterpart.

4. Make your changes *commit early commit often*.
- `git add .` *notice the trailing period
- `git commit -m "message here, in quotations"`

5. Push your changes up to your remote branch.
- `git push`

6. Handle Pull requests through GitLab's GUI.
